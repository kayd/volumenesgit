Python 3.6.4 (v3.6.4:d48eceb, Dec 19 2017, 06:04:45) [MSC v.1900 32 bit (Intel)] on win32
Type "copyright", "credits" or "license()" for more information.
import math
def vol_esf(r):
	import math
	import pi
	if  r>0:
		volumen=(4/3)*pi*r**3
		return volumen
	else:
                return "El valor del radio debe ser mayor que cero"
	
def vol_piramide(ladobase,ancho,altura):
        volumen=(ladobase*ancho*altura)/3
	if ladobase >0 and ancho>0 and altura>0:
		return volumen
	else:
                return "El valor del radio debe ser mayor que cero"

def volumen_cilindro(Radio,Altura):
        if Radio>0 and Altura>0:
                V=math.pi*Radio**2*Altura
                return V
        else:
                return "Los valores tienen que ser mayores que 0"


def area_piramide(LadoBase,Altura):
    """############################################################################
Jarod De la O Segura
Jonathan Esquivel Sánchez
Version:1.0.0
############################################################################"""

    if LadoBase>0 and Altura > 0:
        AL=((4*LadoBase)*math.sqrt((LadoBase/2)**2+Altura**2))/2
        AB=LadoBase*LadoBase
        AT=AL+AB
        return AT
    else:
        return "El lado y la altura tienen que ser mayores que 0"



def area_esfera(Radio):
    """############################################################################
Jarod De la O Segura
Jonathan Esquivel Sánchez
Version:1.0.0
############################################################################"""
    
    if Radio > 0:
        P=4*math.pi*Radio**2
        return P
    else:
        return "El radio tiene que ser mayor que 0"


                

